﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;

namespace MultiThreadApp
{
    class Task1a
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Введите число для расчета его факториала");
            var range = int.Parse(Console.ReadLine());
            Thread thread = new Thread(FactCount) { Name = "Расчет факториала числа", };
            Console.WriteLine("Производится расчет факториала числа");
            thread.Start(range);
            thread.Join();
            Console.ReadKey();
        }

        private static void FactCount(object rangeObj)
        {
            var range = (int) rangeObj;
            BigInteger res = new BigInteger(1);
            for (int i = 2; i <= range; i++)
            {
                res = res * i;
            }
            Console.WriteLine("{0}: {1}! = {2}", Thread.CurrentThread.Name, range.ToString(), res.ToString());
        }
    }
}

    
