﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MultiThreadApp
{
    class Task1b
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Введите число для расчета суммы целых чисел до него");
            var range = long.Parse(Console.ReadLine());
            Thread thread = new Thread(SumToN) { Name = "Сумма целых чисел до числа", };
            Console.WriteLine("Производится расчет суммы целых чисел до введённого числа");
            thread.Start(range);
            thread.Join();
            Console.ReadKey();
        }

        private static void SumToN(object rangeObj)
        {
            var range = (long)rangeObj; 
            long res = 0;
            for (int i = 1; i <= range; i++)
            {
                res = res + i;
            }
            Console.WriteLine("{0} {1} равна {2}", Thread.CurrentThread.Name, range.ToString(), res.ToString());
        }
    }
}


