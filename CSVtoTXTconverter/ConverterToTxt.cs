﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using XML_Converter;

namespace CSVtoTXTconverter
{
    public class ConverterToTxt
    {
        private string inputPath;
        private string outputPath;

        public ConverterToTxt(string inputPath, string outputPath)
        {
            this.inputPath = inputPath;
            this.outputPath = outputPath;
        }

        public void Run()
        {
            var i = new FileStream(inputPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using (Stream f = new FileStream(outputPath, FileMode.OpenOrCreate))
            {
                f.SetLength(0);
                BasicObject.ListToTxt(f, BasicObject.ReadAllCsv(i));
            }
        }
    }
}
