using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;



namespace XML_Converter
{

    public class BasicObject
    {
        public IList<string> values { get; set; }

        public static BasicObject FromCsv(TextReader a)
        {
            var b = a.ReadLine().Split(';');
            return new BasicObject {values = b};
        }

        public static IEnumerable<BasicObject> ReadAllCsv(Stream i)
        {
            var a = new StreamReader(i, Encoding.Default);
            for (; !a.EndOfStream;)
            {
                var t = BasicObject.FromCsv(a);
             //   Thread.Sleep (5000);
                yield return t;
            }
            a.Close();
        }

        public static void ListToTxt(Stream f, IEnumerable<BasicObject> input)
        {
            TextWriter b = new StreamWriter(f, Encoding.Default);
            foreach (var t in input)
            {
                t.ToTxt(b);
            }
            b.Close();
        }

        public void ToTxt(TextWriter Console)
        {
            for (int i = 0; i < values.Count; i++)
            {
                Console.Write(values[i]);
                if (i < values.Count - 1) Console.Write('\t');
            }
            Console.WriteLine();
        }
    }
}