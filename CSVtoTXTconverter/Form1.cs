﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using CSVtoTXTconverter;

namespace XML_Converter
{
    public partial class Form1 : Form
    {
        private const string strFilter =
            "CSV files (*.csv)|*.CSV|Текстовые файлы (*.txt)|*.txt|Все файлы (*.*)|*.*";

        private Task task;

        public Form1()
        {
            InitializeComponent();
        }

        private void Browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog {Filter = strFilter,};
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                SourseFile.Text = dlg.FileName;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            SaveFileDialog dlg = new SaveFileDialog {Filter = strFilter,};
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                OutputFile.Text = dlg.FileName;
            }
        }

        private void Start_Click(object sender, EventArgs e)
        {
            Start_ClickAsync();
        } 
 
        private async Task Start_ClickAsync()
        {
            var c = new ConverterToTxt(SourseFile.Text, OutputFile.Text);
            var oldText = Start.Text;
            Start.Text = "Обработка...";
            Start.Enabled = false;
            task = Task.Factory.StartNew(c.Run);
            await task;
            Start.Text = oldText;
            Start.Enabled = true;
            MessageBox.Show("Обработка завершена");
        }

        private  void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (task != null) task.Wait();
        }

    }
}   
    